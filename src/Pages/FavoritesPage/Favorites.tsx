import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { ButtonOrangeDefault, Card } from 'src/Components';
import { Actions, Selectors } from 'src/redux';
import styles from './styles.module.scss';
import noPicture from '../noPicture.png';

const Favorites:React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { REACT_APP_BASE_IMAGE_URL } = process.env;
  const genres = useSelector(Selectors.genre.getGenresData);
  const allFavorite = useSelector(Selectors.favorite.getAllFavoritesData);

  const buttonGoBack = () => {
    history.goBack();
  };

  const getAllGenres = () => {
    dispatch(Actions.genre.getGenres());
  };

  const onDetailsClick = id => {
    const location = {
      pathname: `/details/${id}`,
    };
    history.push(location);
  };

  const getAllFavorites = useCallback(() => {
    dispatch(Actions.favorite.getFavorite());
  }, []);

  const addFavoriteClick = id => {
    dispatch(Actions.favorite.deleteFavorite(id));
  };

  const addGenres = genreIds => {
    const genreName = [];
    genreIds.filter(el => genres.map(item => {
      if (el === item.id) {
        return genreName.push(item.name);
      }
      return genreName;
    }));
    return genreName;
  };

  useEffect(() => {
    getAllGenres();
    getAllFavorites();
  }, []);

  return (
    <div className={ styles.base_container }>
      <div className={ styles.details_button_go_back }>
        <ButtonOrangeDefault title="На главную" onClick={ buttonGoBack } />
      </div>
      <div className={ styles.card_container }>
        {allFavorite.map(el => {
          const movieGenres = addGenres(el.genreIds);
          return (
            <Card
              id={ el.id }
              voteAverage={ el.voteAverage }
              genre={ movieGenres }
              title={ el.title }
              subtitle={ !el.overview ? 'Описание скоро появится' : el.overview }
              image={ el.backdropPath
                ? `${REACT_APP_BASE_IMAGE_URL}${el.backdropPath}` : noPicture }
              key={ el.id }
              buttonName="Убрать"
              favoriteClick={ () => addFavoriteClick(el.id) }
              detailsClick={ () => onDetailsClick(el.id) }
            />
          );
        })}
      </div>
    </div>
  );
};

export default Favorites;
