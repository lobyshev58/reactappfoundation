import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Filters,
  Card,
  Loader,
  ButtonGreyDefault,
  ButtonOrangeDefault,
  ButtonArrow,
  ETypeTypography,
  Typography,
  SearchEngine,
} from 'src/Components';
import { Actions, Selectors } from 'src/redux';
import { useHistory, useLocation } from 'react-router';
import { ERotateButton } from 'src/Components/Buttons/baseTypes';
import styles from './styles.module.scss';
import noPicture from '../noPicture.png';

type ISelectGenres={
  id:number,
  name:string,
  active:boolean
}

const MainPage: React.FC = () => {
  const { REACT_APP_BASE_IMAGE_URL } = process.env;
  const [ searchMovies, setSearchMovies ] = useState('');
  const [ filterName, setFilterName ] = useState<ISelectGenres[]>([]);
  const dispatch = useDispatch();
  const history = useHistory();
  const genres = useSelector(Selectors.genre.getGenresData);
  const movies = useSelector(Selectors.movie.getMoviesData);
  const pageInfo = useSelector(Selectors.movie.getPageInfo);
  const requestToken = useSelector(Selectors.token.getTokenData);
  const allFavorite = useSelector(Selectors.favorite.getAllFavoritesData);
  const loadingMovie = useSelector(Selectors.movie.getLoadingMovies);
  const loadingGenres = useSelector(Selectors.genre.getLoadingGenres);
  let sessionId = useSelector(Selectors.sessionId.getsessionIdData);

  const queryPage = new URLSearchParams(useLocation().search);
  const page = parseInt(queryPage.get('page'), 10);

  const onFavoritesClick = () => {
    const location = {
      pathname: '/favorites',
    };
    history.push(location);
  };

  const onDetailsClick = id => {
    const location = {
      pathname: `/details/${id}`,
    };
    history.push(location);
  };

  const getMovieGanresAPI = () => {
    dispatch(Actions.genre.getGenres());
  };

  const getRequestTokenAPI = () => {
    dispatch(Actions.token.getRequestToken());
  };

  const clearSearch = () => {
    setSearchMovies('');
    dispatch(Actions.movie.getMovies({ page }));
  };

  const searchMoviesApi = () => {
    if (searchMovies !== '') {
      dispatch(Actions.movie.getSearchMovies({ query: searchMovies, pages: page }));
    }

    if (searchMovies.length === 0) {
      clearSearch();
    }
  };

  const getsessionIdAPI = (urlRequestToken:string) => {
    dispatch(Actions.sessionId.getsessionId({ request_token: urlRequestToken }));
  };

  const findMovieGenres = useCallback(el => {
    const movieGenre = [];
    el.genreIds.map(genreId => genres.map(genre => {
      if (genre.id === genreId) {
        return movieGenre.push(genre.name);
      }
      return movieGenre;
    }));

    return movieGenre;
  }, [ genres ]);

  const getAllFavorites = useCallback(() => {
    dispatch(Actions.favorite.getFavorite());
  }, []);

  const addFavoriteClick = (id, inFavorite, movie) => {
    if (!inFavorite) {
      dispatch(Actions.favorite.postFavorite(movie));
    } else {
      dispatch(Actions.favorite.deleteFavorite(id));
    }
  };

  const onHandleSignIn = () => {
    const redirectLink = 'https://www.themoviedb.org/authenticate/';
    window.location.href = `${redirectLink}${requestToken}?redirect_to=http://localhost:3000/main?page=1`;
  };

  const onHandleSignOut = useCallback(() => {
    localStorage.clear();
    dispatch(Actions.sessionId.clearSessionId());
    history.replace(
      {
        search: '',
      },
    );
    getRequestTokenAPI();
  }, []);

  const handlePaginationClick = (nextPage:number) => {
    if (nextPage >= 1 && pageInfo.totalPage >= nextPage) {
      const location = {
        search: `?page=${nextPage}`,
      };
      history.push(location);
    }
  };

  useEffect(() => {
    const url = new URL(`http://localhost:3000/${history.location.search}`);

    getMovieGanresAPI();
    if (sessionId) {
      getAllFavorites();
    }
    if (url.searchParams.get('request_token')) {
      if (!sessionId) {
        getsessionIdAPI(url.searchParams.get('request_token'));
      }
    } else {
      getRequestTokenAPI();
    }
  }, []);

  const hadeliClickGenres = useCallback((id: number) => {
    setFilterName(prevState => prevState.map(el => ({ ...el, active: (el.id === id ? !el.active : el.active) })));
  }, []);

  const onTagClick = useCallback(name => {
    const genreTagClick = genres.find(el => el.name === name).id;
    if (genreTagClick) {
      dispatch(Actions.movie.getMoviesByGenres({ page, withGenres: `${genreTagClick}` }));
      hadeliClickGenres(genreTagClick);
    }
  }, [ genres ]);

  useEffect(() => {
    const arrayGenresId = [];
    filterName.forEach(el => {
      if (el.active) {
        arrayGenresId.push(el.id);
      }
    });
    dispatch(Actions.movie.getMoviesByGenres({ page, withGenres: `${arrayGenresId}` }));
  }, [ filterName ]);

  useEffect(() => {
    searchMoviesApi();
  }, [ searchMovies ]);

  useEffect(() => {
    dispatch(Actions.movie.getMovies({ page }));
  }, [ page ]);

  useEffect(() => {
    setFilterName(genres.map(el => ({ ...el, active: false })));
  }, [ genres ]);

  if (sessionId === '' || null) {
    sessionId = JSON.parse(localStorage.getItem('sessionId'));
  } else {
    localStorage.setItem('sessionId', JSON.stringify(sessionId));
  }

  return (
    <div>
      <div className={ styles.basic_container }>
        <SearchEngine searchMovies={ searchMovies } setSearchMovies={ setSearchMovies } clearSearch={ clearSearch } />
        <div className={ styles.top_panel }>
          {loadingGenres ? (<Loader />)
            : (
              <div className={ styles.filters_container }>
                <Filters
                  genresFilter={ genres }
                  onHandleClickGenre={ hadeliClickGenres }
                  selectedGenres={ filterName }
                />
              </div>
            )}
          <div className={ styles.button_authentication }>
            <div className={ styles.all_favorite }>
              {(sessionId
                ? <ButtonOrangeDefault title="Выйти" onClick={ onHandleSignOut } />
                : <ButtonOrangeDefault title="Войти" onClick={ onHandleSignIn } />)}
              <ButtonGreyDefault title="Избранное" onClick={ onFavoritesClick } />
            </div>
          </div>
        </div>
        {loadingMovie
          ? (
            <div>
              <div className={ styles.card_container }>
                {movies.map(el => {
                  const movieGenre = findMovieGenres(el);
                  const movieFavorite = allFavorite.some(res => el.id === res.id);
                  return (
                    <Card
                      id={ el.id }
                      voteAverage={ el.voteAverage }
                      genre={ movieGenre }
                      title={ el.title }
                      subtitle={ !el.overview ? 'Описание скоро появится' : el.overview }
                      image={ el.backdropPath
                        ? `${REACT_APP_BASE_IMAGE_URL}${el.backdropPath}` : noPicture }
                      key={ el.id }
                      buttonName={ movieFavorite ? 'Убрать' : 'Добавить' }
                      favoriteClick={ () => addFavoriteClick(el.id, movieFavorite, el) }
                      detailsClick={ () => onDetailsClick(el.id) }
                      tagClick={ onTagClick }
                    />
                  );
                })}
              </div>
              <div className={ styles.pagination }>
                <div className={ styles.pagination_container }>

                  {(page !== 1 ? (

                    <div className={ styles.pagination_extreme_buttons }>
                      <ButtonArrow
                        rotate={ ERotateButton.left }
                        onClick={ () => handlePaginationClick(page - 1) }
                      />
                      <button
                        className={ styles.pagination_button }
                        type="button"
                        onClick={ () => handlePaginationClick(1) }
                      >
                        <Typography
                          text="1"
                          tag={ ETypeTypography.P }
                          textColor="orange"
                        />

                      </button>
                      <div className={ styles.pagination_space }>
                        <Typography
                          text="..."
                          tag={ ETypeTypography.P }
                          textColor="orange"
                        />
                      </div>
                    </div>
                  ) : <div />)}
                  <button type="button" className={ styles.pagination_page }>
                    <Typography
                      text={ `${page}` }
                      tag={ ETypeTypography.P }
                      textColor="black"
                    />
                  </button>
                  {(page !== pageInfo.totalPage ? (
                    <div className={ styles.pagination_extreme_buttons }>
                      <div className={ styles.pagination_space }>
                        <Typography
                          text="..."
                          tag={ ETypeTypography.P }
                          textColor="orange"
                        />
                      </div>
                      <button
                        type="button"
                        className={ styles.pagination_button }
                        onClick={ () => handlePaginationClick(pageInfo.totalPage) }
                      >
                        <Typography
                          text={ `${pageInfo.totalPage}` }
                          tag={ ETypeTypography.P }
                          textColor="orange"
                        />
                      </button>
                      <ButtonArrow onClick={ () => handlePaginationClick(page + 1) } />
                    </div>
                  ) : <div />)}

                </div>
              </div>
            </div>
          ) : (<Loader />) }

      </div>
    </div>
  );
};
export default MainPage;
