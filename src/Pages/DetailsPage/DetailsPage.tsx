import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import { ButtonOrangeDefault, ETypeTypography, Typography } from 'src/Components';
import { Actions, Selectors } from 'src/redux';
import styles from './styles.module.scss';

const DetailsPage = () => {
  const { id }:{id:string} = useParams();
  const { REACT_APP_BASE_IMAGE_URL } = process.env;
  const dispatch = useDispatch();
  const history = useHistory();
  const movieDetails = useSelector(Selectors.movie.getDetailtsMovies);
  const getDetailsMovieAPI = () => {
    dispatch(Actions.movie.getDetailsMovies({ movieId: parseInt(id, 10) }));
  };

  useEffect(() => {
    getDetailsMovieAPI();
  }, []);

  const buttonGoBack = () => {
    history.goBack();
  };

  return (
    <div className={ styles.details_basic_container }>
      <div className={ styles.details_button_go_back }>
        <ButtonOrangeDefault title="На главную" onClick={ buttonGoBack } />
      </div>
      <div className={ styles.details_image_container }>
        <img
          src={ `${REACT_APP_BASE_IMAGE_URL}${movieDetails.backdropPath}` }
          alt="Постер фильма"
          className={ styles.details_image }
        />
      </div>
      <div className={ styles.details_information }>
        <div className={ styles.details_information_title }>
          <Typography text={ movieDetails.title } tag={ ETypeTypography.H1 } textColor="orange" />
        </div>
        <div className={ styles.details_information_keywords }>
          {movieDetails.adult ? (
            <Typography text="18+" tag={ ETypeTypography.H2 } textColor="black" />
          ) : (
            <div className={ styles.details_information_keywords_adult } />
          )}
          <Typography text={ movieDetails.releaseDate } tag={ ETypeTypography.H2 } textColor="black" />
          {movieDetails.genres.map(el => (
            <Typography text={ el.name } tag={ ETypeTypography.H2 } textColor="black" />
          ))}
        </div>
        <div className={ styles.details_information_discription }>
          <Typography text={ movieDetails.overview } tag={ ETypeTypography.P } textColor="white" />
        </div>
      </div>
    </div>
  );
};

export default DetailsPage;
