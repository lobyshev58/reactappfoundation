import React from 'react';
import { useHistory } from 'react-router-dom';
import { ButtonGreyDefault } from 'src/Components';
import styles from './styles.module.scss';

const NoMatch:React.FC = () => {
  const history = useHistory();

  const onLinkClick = () => {
    history.push('/main?page=1');
  };

  return (
    <div className={ styles.base_container }>

      <div>
        <p> Ничего не найдено </p>
      </div>
      <div>
        <ButtonGreyDefault title="Вернуться на главную страницу" onClick={ onLinkClick } />
      </div>
    </div>
  );
};

export default NoMatch;
