import { all, fork } from 'redux-saga/effects';
import { combineReducers } from 'redux';

import moviesReducer, {
  saga as movieSaga,
  actions as movieActions,
  selectors as movieSelectors,
} from './movies';

import genresReducer, {
  saga as genreSaga,
  actions as genreActions,
  selectors as genreSelectors,
} from './genres';

import tokenRducer, {
  saga as tokenSaga,
  actions as tokenActions,
  selectors as tokenSelectors,
} from './requestToken';

import sessionIdReducer, {
  saga as sessionIdSaga,
  actions as sessionIdActions,
  selectors as sessionIdSelectors,
} from './sessionId';

import favoriteReducer, {
  actions as favoriteActions,
  selectors as favoriteSelectors,
} from './favorite';

const rootReducer = combineReducers({
  movie: moviesReducer,
  genre: genresReducer,
  token: tokenRducer,
  sessionId: sessionIdReducer,
  favorite: favoriteReducer,
});

const actions = {
  movie: movieActions,
  genre: genreActions,
  token: tokenActions,
  sessionId: sessionIdActions,
  favorite: favoriteActions,
};

const selectors = {
  movie: movieSelectors,
  genre: genreSelectors,
  token: tokenSelectors,
  sessionId: sessionIdSelectors,
  favorite: favoriteSelectors,
};

function* rootSaga() {
  yield all([ fork(movieSaga) ]);
  yield all([ fork(genreSaga) ]);
  yield all([ fork(tokenSaga) ]);
  yield all([ fork(sessionIdSaga) ]);
}

export * from './genres';
export {
  rootReducer as default, rootSaga, selectors as Selectors, actions as Actions,
};
