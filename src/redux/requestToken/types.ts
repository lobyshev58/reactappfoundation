export const GET_REQUEST_TOKEN = 'GET_REQUEST_TOKEN';
export const PUT_REQUEST_TOKEN = 'PUT_REQUEST_TOKEN';

export interface IRequestTokenState {
  loading: boolean,
  token: string,
}
