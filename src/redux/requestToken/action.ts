import {
  GET_REQUEST_TOKEN, PUT_REQUEST_TOKEN,
} from './types';

export const getRequestToken = () => ({
  type: GET_REQUEST_TOKEN,
});

export const putRequestToken = (token:any) => ({
  type: PUT_REQUEST_TOKEN,
  token,
});
