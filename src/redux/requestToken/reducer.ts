import {
  IRequestTokenState, GET_REQUEST_TOKEN, PUT_REQUEST_TOKEN,
} from './types';

const initialState:IRequestTokenState = {
  loading: false,
  token: '',
};

export default (state = initialState, action:{type:string, token:{data:{requestToken:string}}}) => {
  switch (action.type) {
    case GET_REQUEST_TOKEN:
      return { ...state, loading: false };
    case PUT_REQUEST_TOKEN:
      return ({
        ...state, loading: true, token: action.token.data.requestToken,
      });
    default: return state;
  }
};
