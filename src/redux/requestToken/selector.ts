import { createSelector } from 'reselect';
import { IState } from '../stateTypes';

const stateToken = (state: IState) => state.token;

export const getTokenData = createSelector(stateToken, stateToken => stateToken.token);

export const getLoadingToken = createSelector(stateToken, stateToken => stateToken.loading);
