import {
  takeLatest, put, call, all,
} from 'redux-saga/effects';
import { RequestTokenService } from 'src/Services';
import { putRequestToken } from './action';
import { GET_REQUEST_TOKEN } from './types';

function* workerGetRequestToken():Generator<any> {
  const initTokenService = RequestTokenService.getRequestToken;
  try {
    const data = yield call(() => initTokenService());
    yield put(putRequestToken(data));
  } catch (error) {
    console.log('error getRequestToken');
  }
}

export default function* () {
  yield all([ takeLatest(GET_REQUEST_TOKEN, workerGetRequestToken) ]);
}
