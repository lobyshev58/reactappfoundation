import {
  IsessionIdState, GET_SESSIONS_ID, PUT_SESSIONS_ID, CLEAR_SESSIONS_ID,

} from './types';

const initialState:IsessionIdState = {
  loading: false,
  sessionId: '',
};

export default (state = initialState, action:any) => {
  switch (action.type) {
    case GET_SESSIONS_ID:
      return { ...state, loading: false };
    case PUT_SESSIONS_ID: {
      return ({
        ...state, loading: true, sessionId: action.sessionId.data.sessionId,
      }); }
    case CLEAR_SESSIONS_ID: {
      return ({
        ...state, loading: true, sessionId: '',
      }); }
    default: return state;
  }
};
