import {
  CLEAR_SESSIONS_ID,
  GET_SESSIONS_ID, PUT_SESSIONS_ID,
} from './types';

export const getsessionId = data => ({
  type: GET_SESSIONS_ID,
  payload: data,
});

export const putsessionId = (sessionId:any) => ({
  type: PUT_SESSIONS_ID,
  sessionId,
});

export const clearSessionId = () => ({
  type: CLEAR_SESSIONS_ID,
});
