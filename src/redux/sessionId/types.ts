export const GET_SESSIONS_ID = 'GET_SESSIONS_ID';
export const PUT_SESSIONS_ID = 'PUT_SESSIONS_ID';
export const CLEAR_SESSIONS_ID = 'CLEAR_SESSIONS_ID';

export interface IsessionIdState {
  loading: boolean,
  sessionId: string,
}
