import { createSelector } from 'reselect';
import { IState } from '../stateTypes';

const statesessionId = (state: IState) => (state.sessionId);

export const getsessionIdData = createSelector(statesessionId, statesessionId => statesessionId.sessionId);

export const getLoadingsessionId = createSelector(statesessionId, statesessionId => statesessionId.loading);
