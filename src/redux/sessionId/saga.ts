import {
  takeLatest, put, call, all,
} from 'redux-saga/effects';
import { sessionIdService } from 'src/Services';
import { putsessionId } from './action';
import { GET_SESSIONS_ID } from './types';

function* workerGetsessionId(action):Generator<any> {
  const initsessionIdService = sessionIdService.getsessionId;
  try {
    const data = yield call(() => initsessionIdService(action.payload));
    yield put(putsessionId(data));
  } catch (error) {
    console.log('error getsessionId');
  }
}

export default function* () {
  yield all([ takeLatest(GET_SESSIONS_ID, workerGetsessionId) ]);
}
