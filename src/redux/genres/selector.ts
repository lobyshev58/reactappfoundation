import { createSelector } from 'reselect';
import { IState } from '../stateTypes';

const stateGenres = (state: IState) => state.genre;

export const getGenresData = createSelector(stateGenres, stateGenres => stateGenres.genre);

export const getLoadingGenres = createSelector(stateGenres, stateGenres => stateGenres.loading);
