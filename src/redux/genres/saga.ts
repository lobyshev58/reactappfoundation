import {
  takeLatest, put, call, all,
} from 'redux-saga/effects';
import { GenresService } from 'src/Services';
import { putGenres } from './action';
import { GET_GENRES } from './types';

function* workerGetGenres():Generator<any> {
  const initGenerService = GenresService.getMovieGenres;

  try {
    const data = yield call(() => initGenerService());
    yield put(putGenres(data));
  } catch (error) {
    console.log('error getGenres');
  }
}

export default function* () {
  yield all([ takeLatest(GET_GENRES, workerGetGenres) ]);
}
