import {
  IGenresState,
  GET_GENRES, PUT_GENRES,
} from './types';

const initialState:IGenresState = {
  loading: false,
  genre: [{
    id: 0,
    name: '',
  }],
};

export default (state = initialState, action:any) => {
  switch (action.type) {
    case GET_GENRES:
      return { ...state, loading: true };
    case PUT_GENRES:
      return ({
        ...state, loading: false, genre: action.gener.data.genres,
      });
    default: return state;
  }
};
