export const GET_GENRES = 'GET_GENRES';
export const PUT_GENRES = 'PUT_GENRES';

export interface IGenresState {
  loading: boolean,
  genre: IGenrePart[],
}

export type IGenrePart = {
id: number,
name: string,
}
