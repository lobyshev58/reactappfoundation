import {
  GET_GENRES, PUT_GENRES,
} from './types';

export const getGenres = () => ({
  type: GET_GENRES,
});

export const putGenres = (gener:any) => ({
  type: PUT_GENRES,
  gener,
});
