import { createSelector } from 'reselect';
import { IState } from '../stateTypes';

const stateMovies = (state: IState) => state.movie;

export const getMoviesData = createSelector(stateMovies, stateMovies => stateMovies.movie);

export const getLoadingMovies = createSelector(stateMovies, stateMovies => stateMovies.loading);

export const getDetailtsMovies = createSelector(stateMovies, stateMovies => stateMovies.details);

export const getPageInfo = createSelector(stateMovies, stateMovies => stateMovies.pageInfo);
