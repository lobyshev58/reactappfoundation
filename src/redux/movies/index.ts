import * as selectors from './selector';
import * as actions from './action';

export type { IMoviesState } from './types';
export type { IMoviePart } from './types';
export { default as saga } from './saga';
export { default } from './reducer';
export { actions, selectors };
