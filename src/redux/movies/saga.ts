import {
  takeLatest, put, call, all,
} from 'redux-saga/effects';
import {
  DetailsMoviesServices, MoviesByGenresServices, MoviesService, SearchMoviesServices,
} from 'src/Services';
import { putDetailsMovies, putMovies } from './action';
import {
  GET_DETAILS_MOVIES, GET_MOVIES, GET_MOVIES_BY_GENRES, SEARCH_MOVIES,
} from './types';

function* workerGetMovies(action):Generator<any> {
  const initMoviesServices = MoviesService.getMovies;

  try {
    const data = yield call(() => initMoviesServices(action.payload));
    yield put(putMovies(data));
  } catch (error) {
    console.log('error getPopular');
  }
}

function* workerSearchMovies(action):Generator<any> {
  const initMoviesServices = SearchMoviesServices.searchMovies;
  try {
    const data = yield call(() => initMoviesServices(action.payload));
    yield put(putMovies(data));
  } catch (error) {
    console.log('error searchMovies');
  }
}

function* workerDetailsMovies(action):Generator<any> {
  const initMoviesServices = DetailsMoviesServices.getMoviesDetails;
  try {
    const data = yield call(() => initMoviesServices(action.payload));
    yield put(putDetailsMovies(data));
  } catch (error) {
    console.log('error detailsMovie');
  }
}

function* workerMoviesByGenres(action):Generator<any> {
  const initMoviesServices = MoviesByGenresServices.getMoviesByGenres;
  try {
    const data = yield call(() => initMoviesServices(action.payload));
    yield put(putMovies(data));
  } catch (error) {
    console.log('error get movies by genres');
  }
}

export default function* () {
  yield all([ takeLatest(GET_MOVIES, workerGetMovies) ]);
  yield all([ takeLatest(SEARCH_MOVIES, workerSearchMovies) ]);
  yield all([ takeLatest(GET_DETAILS_MOVIES, workerDetailsMovies) ]);
  yield all([ takeLatest(GET_MOVIES_BY_GENRES, workerMoviesByGenres) ]);
}
