import {
  PUT_MOVIES, START_GET_MOVIES, ERROR_GET_MOVIES, GET_MOVIES,
  IMoviesState,
  PUT_DETAILS_MOVIES,
  GET_PAGE_INFO,
} from './types';

const initialState : IMoviesState = {
  loading: true,
  movie: [
    {
      adult: false,
      backdropPath: '',
      genreIds: [],
      genres: [],
      id: 1,
      originalLanguage: 'en',
      overview: '',
      popularity: 1,
      posterPath: '',
      releaseDate: '',
      title: '',
      voteAverage: 1,
    },
  ],
  details: {
    adult: false,
    backdropPath: '',
    genreIds: [],
    genres: [],
    id: 1,
    originalLanguage: 'en',
    overview: '',
    popularity: 1,
    posterPath: '',
    releaseDate: '',
    title: '',
    voteAverage: 1,
  },
  pageInfo: {
    currentPage: 1,
    totalPage: 1,
  },
};

export default (state = initialState, action:any) => {
  switch (action.type) {
    case START_GET_MOVIES:
      return { ...state, loading: false };
    case GET_MOVIES:
      return { ...state, loading: false };
    case PUT_MOVIES: {
      const newPageInfo = {
        currentPage: action.movie.data.page,
        totalPage: action.movie.data.totalPages,
      };
      return ({
        ...state, loading: true, movie: action.movie.data.results, pageInfo: newPageInfo,
      }); }
    case PUT_DETAILS_MOVIES:
      return { ...state, loading: true, details: action.details.data };
    case GET_PAGE_INFO:
      return { ...state, loading: false };
    case ERROR_GET_MOVIES:
      return { ...state, loading: false };
    default: return state;
  }
};
