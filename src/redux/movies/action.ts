import {
  ERROR_GET_MOVIES,
  GET_DETAILS_MOVIES,
  GET_MOVIES,
  GET_MOVIES_BY_GENRES,
  GET_PAGE_INFO,
  PUT_DETAILS_MOVIES,
  PUT_MOVIES,
  SEARCH_MOVIES,
  START_GET_MOVIES,
} from './types';

export const getMovies = (data: {page: number}) => ({
  type: GET_MOVIES,
  payload: data,
});

export const getMoviesByGenres = (data: {page: number, withGenres:string}) => ({
  type: GET_MOVIES_BY_GENRES,
  payload: data,
});

export const getPageInfo = () => ({
  type: GET_PAGE_INFO,
});

export const getDetailsMovies = (data: {movieId:number}) => ({
  type: GET_DETAILS_MOVIES,
  payload: data,
});

export const getSearchMovies = (data: {query: string, pages:number}) => (
  {
    type: SEARCH_MOVIES,
    payload: data,
  });

export const putMovies = (movie:any) => ({
  type: PUT_MOVIES,
  movie,
});

export const putDetailsMovies = (details:any) => ({
  type: PUT_DETAILS_MOVIES,
  details,
});

export const startGetMovies = () => ({
  type: START_GET_MOVIES,
});

export const errorGetMovies = () => ({
  type: ERROR_GET_MOVIES,
});
