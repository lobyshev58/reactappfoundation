export const GET_MOVIES = 'GET_MOVIES';
export const GET_DETAILS_MOVIES = 'GET_DETAILS_MOVIES';
export const GET_MOVIES_BY_GENRES = 'GET_MOVIES_BY_GENRES';
export const PUT_MOVIES = 'PUT_MOVIES';
export const PUT_DETAILS_MOVIES = 'PUT_DETAILS_MOVIES';
export const SEARCH_MOVIES = 'SEARCH_MOVIES';
export const GET_PAGE_INFO = 'GET_PAGE_INFO';
export const START_GET_MOVIES = 'START_GET_MOVIES';
export const ERROR_GET_MOVIES = 'ERROR_GET_MOVIES';

export interface IMoviesState {
  loading: boolean
  movie: IMoviePart[]
  details:IMoviePart
  pageInfo: {
    currentPage: number
    totalPage: number
  }
}

export type IMoviePart = {
  adult: boolean,
  backdropPath: string | null,
  genreIds: number[],
  genres:{id:number, name:string}[],
  id: number,
  originalLanguage: string,
  overview: string,
  popularity: number,
  posterPath: string,
  releaseDate: string,
  title: string,
  voteAverage: number,
}
