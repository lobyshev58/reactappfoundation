import { IFavoriteState } from './favorite';
import { IGenresState } from './genres';
import { IMoviesState } from './movies';
import { IRequestTokenState } from './requestToken/types';
import { IsessionIdState } from './sessionId/types';

export interface IState{
  movie:IMoviesState
  genre:IGenresState
  token:IRequestTokenState
  sessionId:IsessionIdState
  favorite:IFavoriteState
}
