import { IMoviePart } from '../movies';
import {
  DELETE_FAVORITE,
  GET_FAVORITE, POST_FAVORITE,
} from './types';

export const postFavorite = (movie: IMoviePart) => ({
  type: POST_FAVORITE,
  favorite: movie,
});

export const getFavorite = () => ({
  type: GET_FAVORITE,
});
export const deleteFavorite = movieId => ({
  type: DELETE_FAVORITE,
  payload: movieId,
});
