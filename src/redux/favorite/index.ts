import * as selectors from './selector';
import * as actions from './action';

export type { IFavoriteState } from './types';
export { default } from './reducer';
export { actions, selectors };
