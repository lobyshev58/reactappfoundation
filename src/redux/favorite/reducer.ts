import {
  GET_FAVORITE, POST_FAVORITE, IFavoriteState, DELETE_FAVORITE,
} from './types';

const initialState:IFavoriteState = {
  loading: false,
  favorite: {
    success: false,
  },
  allFavorites: [{
    adult: false,
    backdropPath: '',
    genreIds: [],
    genres: [],
    id: 0,
    originalLanguage: 'en',
    overview: '',
    popularity: 1,
    posterPath: '',
    releaseDate: '',
    title: '',
    voteAverage: 1,
  }],
};

export default (state = initialState, action:any) => {
  switch (action.type) {
    case GET_FAVORITE:
      return {
        ...state, loading: false,
      };
    case POST_FAVORITE: {
      const newFavorite = state.allFavorites.filter(el => (el.id > 0));
      newFavorite.push(action.favorite);

      return { ...state, loading: true, allFavorites: newFavorite }; }
    case DELETE_FAVORITE: {
      const delFavorite = state.allFavorites.filter(el => (el.id !== action.payload));

      return {
        ...state, loading: false, allFavorites: delFavorite,
      }; }
    default: return state;
  }
};
