import { createSelector } from 'reselect';
import { IState } from '../stateTypes';

const stateFavorite = (state: IState) => (state.favorite);

export const getAllFavoritesData = createSelector(stateFavorite, stateFavorite => stateFavorite.allFavorites);

export const getLoadingFavoriteData = createSelector(stateFavorite, stateFavorite => stateFavorite.loading);
