export const GET_FAVORITE = 'GET_FAVORITE';
export const POST_FAVORITE = 'PUT_FAVORITE';
export const DELETE_FAVORITE = 'DELETE_FAVORITE';

export interface IFavoriteState {
  loading: boolean,
  favorite: IFavorite,
  allFavorites: IAllFavorites[]
}

export type IFavorite = {
    success: boolean,
  }

export type IAllFavorites = {
  adult: boolean,
  backdropPath: string | null,
  genreIds: number[],
  genres:{id:number, name:string}[],
  id: number,
  originalLanguage: string,
  overview: string,
  popularity: number,
  posterPath: string,
  releaseDate: string,
  title: string,
  voteAverage: number,
  }
