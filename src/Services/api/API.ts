import axios from 'axios';
import { ELanguage } from '../Language';

enum ERequestsType{
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
}

type QueryTypes={
  method:ERequestsType,
   url:string,
   header?:object,
   body?:object,
   language?:string,
   page?:string,
   requestTokenFlag?:boolean}

let caseFlag;
const toCamelCase = (str:string) => str.split('_').map((el, index) => {
  if (index === 0) {
    return el;
  }
  return el.charAt(0).toUpperCase() + el.slice(1);
}).join('');

const toSnakeCase = str => str.replace(/[A-Z]/g, el => `_${el.toLowerCase()}`);

const checkData = (object: object) => {
  let res = {};
  if (typeof object !== 'object') {
    return object;
  }
  Object.keys(object).forEach(el => {
    if (object[el] == null) {
      if (caseFlag) {
        res = { ...res, [toSnakeCase(el)]: null };
      } else {
        res = { ...res, [toCamelCase(el)]: null };
      }
    } else {
      if (typeof object[el] === 'object') {
        if (caseFlag) {
          res = { ...res, [toSnakeCase(el)]: checkData(object[el]) };
        } else {
          res = { ...res, [toCamelCase(el)]: checkData(object[el]) };
        }
      }
      if (typeof object[el] === 'string' || typeof object[el] === 'boolean' || typeof object[el] === 'number') {
        if (caseFlag) {
          res = { ...res, [toSnakeCase(el)]: object[el] };
        } else {
          res = { ...res, [toCamelCase(el)]: object[el] };
        }
      }
      if (Array.isArray(object[el])) {
        if (caseFlag) {
          res = { ...res, [toSnakeCase(el)]: object[el].map(checkData) };
        } else {
          res = { ...res, [toCamelCase(el)]: object[el].map(checkData) };
        }
      }
    }
  });

  return res;
};

export class API {
   baseUrl:string

   language: string = ELanguage.RU

   apiKey:string;

   constructor(baseUrl:string, apiKey: string) {
     this.baseUrl = baseUrl;
     this.apiKey = apiKey;
   }

   setLang(language:string) {
     this.language = language;
   }

   query({
     method, url, header, body,
   }:QueryTypes) {
     return axios(`${this.baseUrl}${url}api_key=${this.apiKey}`, {
       method,
       headers: {
         'Content-type': 'application/json',
         ...header,
       },
       params: { language: this.language, ...body },
     }).then(res => { caseFlag = false; return (checkData(res)); });
   }

   queryGET(url:string, data: any) {
     caseFlag = true;
     const newData = checkData(data);
     return this.query({
       method: ERequestsType.GET, url, body: newData,
     });
   }

   queryPOST(url:string, body:object, header?:object) {
     caseFlag = true;
     return this.query({
       method: ERequestsType.POST, url, header, body,
     });
   }

   queryPATCH(url:string, body:object, header?:object) {
     caseFlag = true;
     return this.query({
       method: ERequestsType.PATCH, url, header, body,
     });
   }

   queryDELETE(url:string, header?:object) {
     return this.query({
       method: ERequestsType.DELETE, url, header,
     });
   }
}
