import { API } from '../api';

export class Genres {
  apiService: API

  url = '/genre/movie/list?'

  constructor(apiServise: API) {
    this.apiService = apiServise;
  }

  getMovieGenres = () => (this.apiService.queryGET(this.url, undefined))
}
