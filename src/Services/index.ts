import { API } from './api';
import { DetailsMovies } from './DetailsMovies/DetilsMovies';
import { Genres } from './Genres';
import { MoviesByGenres } from './MovieByGenres';
import { MoviesPopular } from './MoviesPopular';
import { RequestToken } from './RequestToken/RequestToken';
import { SearchMovies } from './SearchMovies';
import { SessionId } from './SessionsId/SessionsId';

const { REACT_APP_API_KEY, REACT_APP_BASE_URL } = process.env;

export const ApiServices = new API(REACT_APP_BASE_URL || '', REACT_APP_API_KEY || '');

const MoviesService = new MoviesPopular(ApiServices);

const RequestTokenService = new RequestToken(ApiServices);

const sessionIdService = new SessionId(ApiServices);

const GenresService = new Genres(ApiServices);

const SearchMoviesServices = new SearchMovies(ApiServices);

const DetailsMoviesServices = new DetailsMovies(ApiServices);

const MoviesByGenresServices = new MoviesByGenres(ApiServices);

export * from './api';

export {
  MoviesService,
  RequestTokenService,
  GenresService,
  SearchMoviesServices,
  DetailsMoviesServices,
  sessionIdService,
  MoviesByGenresServices,
};
