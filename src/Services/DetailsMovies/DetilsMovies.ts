import { API } from '../api';

export class DetailsMovies {
  apiService: API

  url = '/movie/'

  constructor(apiServise: API) {
    this.apiService = apiServise;
  }

  getMoviesDetails= (data: {movieId: number}) => (this.apiService.queryGET(`${this.url + data.movieId}?`, undefined));
}
