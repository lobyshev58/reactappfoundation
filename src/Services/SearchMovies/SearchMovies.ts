import { API } from '../api';

export class SearchMovies {
  apiService: API

  url = '/search/movie?'

  constructor(apiServise: API) {
    this.apiService = apiServise;
  }

  searchMovies= (data: {query: string, pages:number}) => (this.apiService.queryGET(this.url, data))
}
