import { API } from '../api';

export class MoviesPopular {
  apiService: API

  url = '/movie/popular?'

  constructor(apiServise: API) {
    this.apiService = apiServise;
  }

  getMovies= (data: {page: number}) => (this.apiService.queryGET(this.url, data))
}
