import { API } from '../api';

export class SessionId {
  apiService: API

   url = '/authentication/session/new?'

   constructor(apiServise: API) {
     this.apiService = apiServise;
   }

   getsessionId = (data: {requestToken: string}) => (this.apiService.queryPOST(this.url, data))
}
