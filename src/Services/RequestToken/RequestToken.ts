import { API } from '../api';

export class RequestToken {
  apiService: API

   url = '/authentication/token/new?'

   constructor(apiServise: API) {
     this.apiService = apiServise;
   }

   getRequestToken = () => (this.apiService.queryGET(this.url, undefined))
}
