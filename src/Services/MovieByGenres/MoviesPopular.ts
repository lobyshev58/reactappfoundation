import { API } from '../api';

export class MoviesByGenres {
  apiService: API

  url = '/discover/movie?'

  constructor(apiServise: API) {
    this.apiService = apiServise;
  }

  getMoviesByGenres= (data: {page: number, withGenres:string}) => (this.apiService.queryGET(this.url, data))
}
