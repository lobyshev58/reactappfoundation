/// SAGA

import {
  createStore,
  applyMiddleware,
  compose,
} from 'redux';
import persistReducer from 'redux-persist/es/persistReducer';
import persistStore from 'redux-persist/es/persistStore';
import createSagaMiddleware from 'redux-saga';
import rootReducer, { rootSaga } from 'src/redux';
import storage from 'redux-persist/lib/storage';

export default function configureStore(initialState) {
  const sagaMiddleware = createSagaMiddleware({});

  const appliedMiddlewares = [
    sagaMiddleware,
  ];
  const persistConfig = {
    key: 'favorite',
    storage,
    whitelist: [ 'favorite' ],
  };

  const reduxDevTool = typeof window === 'object' && window.REDUX_DEVTOOLS_EXTENSION_COMPOSE;
  const composeWithDevTools = !reduxDevTool ? compose : window.REDUX_DEVTOOLS_EXTENSION_COMPOSE({});

  const middlewares = composeWithDevTools(applyMiddleware(...appliedMiddlewares));

  const pReducer = persistReducer(persistConfig, rootReducer);

  const store = createStore(pReducer, initialState, middlewares);

  sagaMiddleware.run(rootSaga);

  const persistor = persistStore(store);

  return { store, persistor };
}
