import React from 'react';

import styles from './styles.module.scss';
import close from './close.svg';

type SearchProps={
  searchMovies: string,
  setSearchMovies: (string)=>void,
  clearSearch: ()=> void
}

const SearchEngine:React.FC<SearchProps> = ({ searchMovies, setSearchMovies, clearSearch }: SearchProps) => (
  <div className={ styles.basic_search_container }>
    <div className={ styles.search_container }>
      <div className={ styles.search_line_container }>
        <input
          placeholder="Найти фильм"
          value={ searchMovies }
          className={ styles.search_line }
          onChange={ e => setSearchMovies(e.target.value) }
        />
      </div>
      <button type="button" className={ styles.close_btn } onClick={ clearSearch }>
        <img src={ close } alt="Close" className={ styles.close_img } />
      </button>
    </div>
  </div>
);
export default SearchEngine;
