import React, { useCallback } from 'react';
import { Typography, ETypeTypography } from '../Typography';
import styles from './styles.module.scss';

type Props = {
  name: string,
  onHandleTagClick?:(name: string)=>void
}
const CardTag: React.FC<Props> = ({ name, onHandleTagClick }) => {
  const onPress = useCallback(() => {
    onHandleTagClick(name);
  }, [ onHandleTagClick ]);
  return (
    <button className={ styles.tag_movie } type="button" onClick={ onPress }>
      <Typography text={ name } tag={ ETypeTypography.H3 } textColor="white" />
    </button>
  );
};

export default CardTag;
