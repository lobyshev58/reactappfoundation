import React from 'react';
import styles from './styles.module.scss';
import { ButtonDetails, ButtonFavorites } from '../Buttons';
import CardTag from './CardTag';
import { Typography, ETypeTypography } from '../Typography';

type Props = {
  title: string
  subtitle: string
  voteAverage?: number
  genre?: string[]
  image?: string
  id: number
  buttonName?:string
  favoriteClick?: ()=>void
  detailsClick?: ()=>void
  tagClick?: (el:string)=> void
}

const Card: React.FC<Props> = ({
  title, subtitle, voteAverage, genre, image, id, buttonName, favoriteClick, detailsClick, tagClick,
}) => (
  <div className={ styles.card_container }>
    <div className={ styles.card }>
      <div className={ styles.image_box }>
        <img src={ image } alt="" className={ styles.card_img } />
      </div>
      <div className={ styles.bottom_details }>
        <div className={ styles.description_container }>
          <div className={ styles.title_film }>
            <Typography text={ title } tag={ ETypeTypography.H1 } textColor="white" />
          </div>
          <div className={ styles.container_tag }>
            {voteAverage ? (
              <div>
                <CardTag name={ `${voteAverage}` } key={ id + voteAverage } />
              </div>
            ) : <div />}
            {genre?.map(el => (
              <div key={ id + el }>
                <CardTag name={ el } onHandleTagClick={ tagClick } />
              </div>
            ))}
          </div>
          <div className={ styles.subtitle }>
            <Typography text={ subtitle } tag={ ETypeTypography.H2 } textColor="white" />
          </div>
        </div>
        <div className={ styles.card_buttons_panel }>
          <ButtonDetails title="Детали" onClick={ detailsClick } />
          <ButtonFavorites title={ buttonName } onClick={ favoriteClick } />
        </div>
      </div>
    </div>
  </div>
);

export default Card;
