import React from 'react';
import { IGenrePart } from 'src/redux';
import styles from './styles.module.scss';
import { ButtonDefault, IFilterNameProps } from '../Buttons/ButtonDefault';

type GenreProps={
    genresFilter:IGenrePart[]
    selectedGenres: IFilterNameProps[]
    onHandleClickGenre:(id:number)=>void
}

const Filters:React.FC<GenreProps> = ({ genresFilter, onHandleClickGenre, selectedGenres }) => (
  <div className={ styles.filters }>
    {genresFilter.map(el => (
      <ButtonDefault
        selectedGenres={ selectedGenres }
        onHandleClickGenre={ onHandleClickGenre }
        element={ el }
        key={ el.id }
      />
    ))}
  </div>
);
export default Filters;
