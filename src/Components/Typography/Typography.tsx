import React from 'react';
import classNames from 'classnames';
import styles from './styles.module.scss';
import 'src/Colors/Colors.scss';

type Props ={
  text: string
  tag: ETypeTypography
  textColor?: string
}

export enum ETypeTypography {
  H1 = 'h1',
  H2 = 'h2',
  H3 = 'h3',
  TITLE = 'title',
  P = 'p',
}

export const Typography: React.FC<Props> = ({
  text, tag,
  textColor = 'white',
}) => (
  <div className={ classNames(styles.base_class, styles[tag], styles[textColor]) }>
    {text}
  </div>
);
