import React from 'react';
import styles from './styles.module.scss';

const Loader:React.FC = () => (
  <div id="container" className={ styles.loader }>
    <div className={ styles.divider } aria-hidden="true" />
    <p className={ styles.loading_text } aria-label="Loading">
      <span className={ styles.letter } aria-hidden="true">L</span>
      <span className={ styles.letter } aria-hidden="true">o</span>
      <span className={ styles.letter } aria-hidden="true">a</span>
      <span className={ styles.letter } aria-hidden="true">d</span>
      <span className={ styles.letter } aria-hidden="true">i</span>
      <span className={ styles.letter } aria-hidden="true">n</span>
      <span className={ styles.letter } aria-hidden="true">g</span>
    </p>
  </div>
);

export default Loader;
