import React from 'react';
import { Typography, ETypeTypography } from 'src/Components';
import classNames from 'classnames';
import styles from './styles.module.scss';
import buttonBadge from './button.svg';
import ButtonProps from '../baseTypes';

const ButtonArrow: React.FC<ButtonProps> = ({ title, onClick, rotate }) => (
  <button type="button" className={ styles.button } onClick={ onClick }>
    <span className={ classNames(styles.span, styles[rotate]) }>
      <img src={ buttonBadge } alt="" />
    </span>
    <Typography
      text={ title }
      tag={ ETypeTypography.P }
      textColor="black"
    />
  </button>
);

export default ButtonArrow;
