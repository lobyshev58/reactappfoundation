import React from 'react';
import { Typography, ETypeTypography } from 'src/Components';
import { IGenrePart } from 'src/redux';
import styles from './styles.module.scss';

type IButtonDefault={
  element: IGenrePart;
  selectedGenres: IFilterNameProps[]
  onHandleClickGenre:(id:number)=>void
}

export type IFilterNameProps={
  id:number,
  name:string,
  active: boolean
  }

const Button: React.FC<IButtonDefault> = ({ selectedGenres, onHandleClickGenre, element }) => (
  <div className={ styles.element }>
    <button
      type="button"
      className={ styles.element_btn }
      onClick={ () => onHandleClickGenre(element.id) }
    >
      <Typography
        tag={ ETypeTypography.P }
        text={ element.name }
        textColor={ selectedGenres.find(item => item.active === true && item.id === element.id)
          ? 'orange' : 'white' }
      />
    </button>
  </div>
);

export default Button;
