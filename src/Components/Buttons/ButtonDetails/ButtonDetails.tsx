import React from 'react';
import { Typography, ETypeTypography } from 'src/Components';
import styles from './styles.module.scss';
import buttonBadge from './button.svg';
import ButtonProps from '../baseTypes';

const Button: React.FC<ButtonProps> = ({ title, onClick }) => (
  <button type="button" className={ styles.button } onClick={ () => onClick() }>
    <span className={ styles.span }>
      <img src={ buttonBadge } alt="" />
    </span>
    <Typography
      text={ title }
      tag={ ETypeTypography.P }
      textColor="black"
    />
  </button>
);

export default Button;
