import React from 'react';
import { Typography, ETypeTypography } from 'src/Components';
import styles from './styles.module.scss';
import ButtonProps from '../baseTypes';

const Button: React.FC<ButtonProps> = ({ title, onClick }) => (
  <button
    onClick={ onClick }
    type="button"
    className={ styles.button }
  >
    <Typography text={ title } tag={ ETypeTypography.P } />
    {' '}
  </button>
);

export default Button;
