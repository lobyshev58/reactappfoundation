import React from 'react';
import { Typography, ETypeTypography } from 'src/Components';
import styles from './styles.module.scss';
import ButtonProps from '../baseTypes';

const Button: React.FC<ButtonProps> = ({ title, onClick }) => (
  <button type="button" className={ styles.button } onClick={ onClick }>
    <Typography text={ title } tag={ ETypeTypography.P } textColor="black" />
  </button>
);

export default Button;
