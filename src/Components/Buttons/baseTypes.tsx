 type ButtonProps = {
  title?: string,
  onClick?: () => void
  rotate?: ERotateButton

}

export enum ERotateButton {
  up = 'up',
  right = 'right',
  down = 'down',
  left = 'left',
}
export default ButtonProps;
