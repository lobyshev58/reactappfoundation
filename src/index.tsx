import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { NoMatch, MainPage, DetailsPage } from './Pages';
import configureStore from './store/store';
import { Favorites } from './Pages/FavoritesPage';

const { store } = configureStore();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={ store }>
      <Router>
        <Switch>
          <Route exact path="/main">
            <MainPage />
          </Route>
          <Route path="/details/:id">
            <DetailsPage />
          </Route>
          <Route path="/favorites">
            <Favorites />
          </Route>
          <Route path="*">
            <NoMatch />
          </Route>

        </Switch>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);
